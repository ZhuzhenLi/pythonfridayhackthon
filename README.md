## Topic: Analyse an apache log file and get statistics from it

a. Produce a report in Microsoft excel (xlsx) or csv showing

b. Errors, notices, forbidden access, most common & least common functions

c. Plot some graphs showing the servers usage profile.

d. You could use many python packages for this one, e.g. pandas, numpy, re, os and
there are many plotting and charting packages – e.g. seaborn, plotly, dash